#include <stdio.h>

extern unsigned long call32_from_64(void *stack, void (*function)(void));

asm (".pushsection .text\n\t"
     ".code32\n\t"
     "syscall32:\n\t"
     "mov $20,%eax\n\t"  // NR_getpid (32-bit)
     "syscall\n\t"
     "syscall32_ret:\n\t"
     "ret\n\t"
     ".code64");
extern void syscall32(void);

static char low_stack[4096];

int main()
{
	unsigned long ret = call32_from_64(low_stack, syscall32);
	printf("syscall return = %ld\n", (long)ret);
	return 0;
}
