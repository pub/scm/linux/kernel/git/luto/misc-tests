#define __STDC_FORMAT_MACROS

#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <err.h>

#define NPAGES 512
#define PAGE_SIZE 4096
static uint64_t faults = 0;

static void iter(void)
{
	clock_getres(CLOCK_REALTIME, (void *)8);
	faults++;
}

int main(int argc, char **argv)
{
	// Warm up
	for (int i = 0; i < 1000000; i++)
		iter();
	faults = 0;

	struct timespec start;
	clock_gettime(CLOCK_MONOTONIC, &start);

	for (int i = 0; i < 1000000; i++)
		iter();

	struct timespec end;
	clock_gettime(CLOCK_MONOTONIC, &end);
	unsigned long long duration = (end.tv_nsec - start.tv_nsec) + 1000000000ULL * (end.tv_sec - start.tv_sec);

	printf("%ld faults in %.5fs = %.2f nsec / loop\n",
	       (long)faults, (float)duration * 1e-9f,
	       (float)duration / faults);
	return 0;
}
