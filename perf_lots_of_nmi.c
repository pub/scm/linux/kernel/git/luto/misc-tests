#include <linux/perf_event.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s CPU\n", argv[0]);
		return 1;
	}

	int cpu = atoi(argv[1]);

	struct perf_event_attr attr;
	memset(&attr, 0, sizeof(attr));

	attr.type = PERF_TYPE_HARDWARE;
	attr.size = sizeof(struct perf_event_attr);
	attr.freq = 1;
	attr.sample_freq = 10000;
	attr.config = PERF_COUNT_HW_CPU_CYCLES;

	int fd = syscall(
		SYS_perf_event_open,
		&attr,			/* attributes */
		-1,			/* monitor all tasks */
		cpu,			/* selected CPU */
		-1,			/* group leader */
		PERF_FLAG_FD_CLOEXEC	/* flags */
		);

	if (fd == -1)
		err(1, "perf_event_open");

	pause();

	/*
	void *addr = mmap(NULL, PAGE_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		err(1, "mmap");
	}
	*/

	return 0;
}
