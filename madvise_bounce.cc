#define __STDC_FORMAT_MACROS

#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <atomic>
#include <signal.h>
#include <pthread.h>
#include <err.h>
#include <sys/mman.h>

void describe_clock(const char *name, int id)
{
	struct timespec res;
	int ret = clock_getres(id, &res);
	if (ret < 0) {
		printf("  %d (%s)  [failed to query resolution]\n",
		       id, name);
	} else {
		printf("  %d (%s)  resolution = %" PRIu64 ".%09u\n",
		       id, name,
		       (uint64_t)res.tv_sec, (unsigned)res.tv_nsec);
	}
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("Usage: %s <iters>\n", argv[0]);

		return 1;
	}

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	int cpu = 0;

	long loop_mult = 1;
	char *loop_arg = strdup(argv[1]);
	size_t loop_arg_len = strlen(loop_arg);
	if (loop_arg_len && loop_arg[loop_arg_len-1] == 'k') {
		loop_mult = 1000;
		loop_arg[loop_arg_len-1] = '\0';
	} else if (loop_arg_len && loop_arg[loop_arg_len-1] == 'M') {
		loop_mult = 1000000;
		loop_arg[loop_arg_len-1] = '\0';
	}
	size_t loops = (size_t)atol(argv[1]) * loop_mult;
	free(loop_arg);

	char *buf = (char *)mmap(0, 4096, PROT_READ|PROT_WRITE,
				 MAP_ANONYMOUS | MAP_PRIVATE | MAP_POPULATE,
				 -1, 0);
	if (buf == MAP_FAILED)
		err(1, "mmap");

	timespec start;
	clock_gettime(CLOCK_MONOTONIC, &start);

	for (size_t i = 0; i < loops; ++i) {
		madvise(buf, 4096, MADV_DONTNEED);
		buf[0] = 1;

		cpu = !cpu;
		CPU_SET(cpu, &cpuset);
		if (sched_setaffinity(0, sizeof(cpuset), &cpuset) != 0)
			err(1, "sched_setaffinity");
		CPU_CLR(cpu, &cpuset);
	}

	timespec end;
	clock_gettime(CLOCK_MONOTONIC, &end);
	unsigned long long duration = (end.tv_nsec - start.tv_nsec) + 1000000000ULL * (end.tv_sec - start.tv_sec);
	printf("%ld loops in %.5fs = %.2f nsec / loop\n",
	       (long)loops, float(duration) * 1e-9,
	       float(duration) / loops);
	if (duration == 0)
		printf("[WARN]\tThe apparent elapsed time was exactly 0.  You have precision issues.\n");
	return 0;
}
