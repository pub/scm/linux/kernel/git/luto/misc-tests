#!/bin/bash

ncpus=`cat /proc/cpuinfo |grep 'model name' |wc -l`
for i in `seq 0 $(($ncpus-1))`; do
    for j in `seq 0 $(($ncpus-1))`; do
	if [[ "$i" != "$j" ]]; then
	    echo -n "$i,$j: "
	    ./evil-clock-test -N -p $i,$j |grep 'Now test'
	fi
    done
done
