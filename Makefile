.PHONY: all clean

SIMPLE_C_TARGETS := dump-vsyscall context_switch_latency kernel_pf user_visible_state null_seccomp highsys gsbase perf_lots_of_nmi

SIMPLE_CC_TARGETS := evil-clock-test

SPLIT_C_TARGETS := dump-vdso dump-vvar syscall_exit_regs dump_all_pmcs ntflag sysret_ss_attrs
SPLIT_CC_TARGETS := timing_test test_vsyscall test_vdso_parser madvise_bounce

ALL_TARGETS := $(SIMPLE_C_TARGETS) $(SIMPLE_CC_TARGETS) $(SPLIT_C_TARGETS:%=%_64) $(SPLIT_CC_TARGETS:%=%_64) $(SPLIT_C_TARGETS:%=%_32) $(SPLIT_CC_TARGETS:%=%_32) syscall32_from_64 segregs

CFLAGS := -O2 -g -std=gnu99 -pthread -Wall
CCFLAGS := -O2 -g -std=gnu++11 -pthread -Wall

all: $(ALL_TARGETS)

clean:
	rm -f $(ALL_TARGETS)

$(SIMPLE_C_TARGETS): %: %.c
	gcc -o $@ $(CFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

segregs: segregs.c
	gcc -o $@ $(CFLAGS) $(EXTRA_CFLAGS) -m32 $^ -lrt -ldl

$(SIMPLE_CC_TARGETS): %: %.cc
	g++ -o $@ $(CCFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

$(SPLIT_C_TARGETS:%=%_32): %_32: %.c
	gcc -m32 -o $@ $(CFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

$(SPLIT_C_TARGETS:%=%_64): %_64: %.c
	gcc -m64 -o $@ $(CFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

$(SPLIT_CC_TARGETS:%=%_32): %_32: %.cc
	g++ -m32 -o $@ $(CCFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

$(SPLIT_CC_TARGETS:%=%_64): %_64: %.cc
	g++ -m64 -o $@ $(CCFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

syscall32_from_64: syscall32_from_64.c thunks.S
	gcc -m64 -o $@ $(CFLAGS) $(EXTRA_CFLAGS) $^ -lrt -ldl

sysret_ss_attrs_64: thunks.S
