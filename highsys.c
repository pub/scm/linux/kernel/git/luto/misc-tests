#include <stdio.h>
#include <sys/syscall.h>

static long high_getpid(void)
{
  long ret;
  asm volatile ("syscall" :
		"=a" (ret) :
		"a" (SYS_getpid | 0xbaadf00d00000000ULL) :
		"memory", "cc", "rcx", "r11");
  return ret;
}

int main()
{
  printf("high_getpid says %ld\n", high_getpid());
  return 0;
}
