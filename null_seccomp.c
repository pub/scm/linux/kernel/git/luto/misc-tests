#include <unistd.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <sys/syscall.h>
#include <err.h>
#include <sys/prctl.h>
#include <stddef.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	struct sock_filter filter[] = {
		BPF_STMT(BPF_RET+BPF_K, SECCOMP_RET_ALLOW),
	};

	struct sock_fprog prog = {
		.len = (unsigned short)(sizeof(filter)/sizeof(filter[0])),
		.filter = filter,
	};

	if (argc < 2) {
		printf("Usage: null_seccomp PATH ARGS...\n");
		return 1;
	}

	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0))
		err(1, "PR_SET_NO_NEW_PRIVS");
	if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, &prog))
		err(1, "PR_SET_SECCOMP");

	execv(argv[1], argv + 1);
	err(1, argv[1]);
}
