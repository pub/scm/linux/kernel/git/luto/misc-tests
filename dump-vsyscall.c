#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main()
{
  /* The kernel thinks that the vsyscall address is no good. */
  char buf[4096];
  memcpy(buf, (void *)0xffffffffff600000, 4096);
  int ret = write(1, buf, 4096);
  if (ret < 0) {
    perror("write");
    return 1;
  } else if (ret != 4096) {
    fprintf(stderr, "output truncated -- whoops\n");
    return 1;
  }

  return 0;
}
